import React, { Component, Fragment } from 'react';
import { Route, Switch, BrowserRouter, Link } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

const PageHome = () => (<div className='PageHome'>HOME PAGE</div>)
const PageAbout = () => (<div className='PageAbout'>ABOUT PAGE</div>)

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <BrowserRouter basename={process.env.PUBLIC_URL || '/'}>
          <Fragment>
            <Link to='/'>Home</Link>
            <Link to='/about'>About</Link>
            <Switch>
              <Route exact path='/' component={PageHome} />
              <Route exact path='/about' component={PageAbout} />
            </Switch>
          </Fragment>
        </BrowserRouter>

      </div>
    );
  }
}

export default App;
